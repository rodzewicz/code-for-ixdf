<?php

use Illuminate\Support\Facades\Route;

/*** REMOVED DUE TO THE NDA ***/
// I removed most of the links due to the NDA. I left only those responsible
// for the execution of the code that I would like to present.

Route::namespace('Api')->prefix('v1')->group(function () {
    Route::get('polices/{policy}/price', 'PolicyPriceController@show');
    Route::post('polices/{policy}/offers', 'PolicyOfferController@store');
    Route::post('insurances', 'InsuranceController@store');
    Route::delete('insurances/{insurance}', 'InsuranceController@destroy');
});
