<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InternalApiException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Policy\ShowPriceRequest;
use App\Http\Resources\PolicyPriceResource;
use App\Models\Policy;
use App\Services\PolicyPriceService;

class PolicyPriceController extends Controller
{
    /**
     * Makes an API call to given insurer's server to get price of the policy
     *
     * @param Policy $policy
     * @param ShowPriceRequest $request
     * @param PolicyPriceService $policyPriceService
     * @return PolicyPriceResource
     * @throws InternalApiException
     */
    public function show(Policy $policy, ShowPriceRequest $request, PolicyPriceService $policyPriceService)
    {
        return new PolicyPriceResource(
            $policyPriceService->get($policy, $request)
        );
    }
}
