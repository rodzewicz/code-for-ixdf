<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InternalApiException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Insurance\DestroyRequest;
use App\Http\Requests\Insurance\StoreRequest;
use App\Http\Resources\InsuranceResource;
use App\Models\Insurance;
use App\Services\InsuranceService;
use Illuminate\Http\JsonResponse;

class InsuranceController extends Controller
{
    /**
     * Makes an API call to given insurer's server to make insurance,
     * save output into database end return saved output as a Resource
     *
     * @param StoreRequest $request
     * @param InsuranceService $insuranceService
     * @return InsuranceResource
     * @throws InternalApiException
     */
    public function store(StoreRequest $request, InsuranceService $insuranceService)
    {
        return new InsuranceResource(
            $insuranceService->create($request)
        );
    }

    /**
     * Makes an API call to given insurer's server to cancel insurance and remove it from database
     *
     * @param Insurance $insurance
     * @param DestroyRequest $request
     * @param InsuranceService $insuranceService
     * @return JsonResponse
     * @throws InternalApiException
     */
    public function destroy(Insurance $insurance, DestroyRequest $request, InsuranceService $insuranceService)
    {
        $insuranceService->destroy($insurance, $request);

        return new JsonResponse(null, 204);
    }
}
