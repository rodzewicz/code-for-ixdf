<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Policy\StoreOfferRequest;
use App\Http\Resources\PolicyOfferResource;
use App\Models\Policy;
use App\Services\OfferService;
use App\Exceptions\InternalApiException;

class PolicyOfferController extends Controller
{
    /**
     * Makes an API call to given insurer's server to make policy offer,
     * save output into database end return saved output as a Resource
     *
     * @param Policy $policy
     * @param StoreOfferRequest $request
     * @param OfferService $offerService
     * @return PolicyOfferResource
     * @throws InternalApiException
     */
    public function store(Policy $policy, StoreOfferRequest $request, OfferService $offerService)
    {
        return new PolicyOfferResource(
            $offerService->create($policy, $request)
        );
    }
}
