<?php


namespace App\Insurers\Europa;


use App\Contracts\InsurerRequestEncoder;
use App\Http\Requests\Insurance\DestroyRequest;
use App\Http\Requests\Insurance\StoreRequest;
use App\Http\Requests\Policy\ShowPriceRequest;
use App\Http\Requests\Policy\StoreOfferRequest;
use App\Models\Insurance;
use App\Models\Offer;
use App\Models\Policy;

class EuropaRequestEncoder implements InsurerRequestEncoder
{
    /**
     * @param Policy $policy
     * @param ShowPriceRequest $request
     * @return array|mixed
     */
    public function encodeForGettingPolicyPrice(Policy $policy, ShowPriceRequest $request)
    {
        $insurerRequest = [];

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API request structure which is part of NDA
        // Removed part of code prepares $insurerRequest based on data from $policy and from $request
        // $insurerRequest may be an array, json string or XML depending on the insurer API
        // Some fake data below:

        $insurerRequest['policy_code'] = $policy->code;
        $insurerRequest['policy_valiant'] = $policy->variant;
        $insurerRequest['dates']['first_day'] = $request->entry_date;
        $insurerRequest['dates']['last_day'] = $request->departure_date;
        // etc...

        return $insurerRequest;
    }

    /**
     * @param Policy $policy
     * @param StoreOfferRequest $request
     * @return array|mixed
     */
    public function encodeForCreatingPolicyOffer(Policy $policy, StoreOfferRequest $request)
    {
        $insurerRequest = [];

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API request structure which is part of NDA
        // Removed part of code prepares $insurerRequest based on data from $policy and from $request
        // $insurerRequest may be an array, json string or XML depending on the insurer API
        // Some fake data below:

        $insurerRequest['policy_code'] = $policy->code;
        $insurerRequest['policy_valiant'] = $policy->variant;
        $insurerRequest['dates']['first_day'] = $request->entry_date;
        $insurerRequest['dates']['last_day'] = $request->departure_date;
        foreach($request->insureds as $insured) {
            $insurerRequest['insureds'][] = [
                'first_name' => $insured['first_name'],
                'last_name' => $insured['last_name'],
                'dob' => $insured['birth_date'],
            ];
        }
        // etc...

        return $insurerRequest;
    }

    /**
     * @param Offer $offer
     * @param StoreRequest $request
     * @return array|mixed
     */
    public function encodeForCreatingInsurance(Offer $offer, StoreRequest $request)
    {
        $insurerRequest = [];

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API request structure which is part of NDA
        // Removed part of code prepares $insurerRequest based on data from $request. It may also refer
        // to Offer object created earlier.
        // $insurerRequest may be an array, json string or XML depending on the insurer API
        // Some fake data below:

        $insurerRequest['offer_number'] = $offer->number;

        return $insurerRequest;
    }

    /**
     * @param Insurance $insurance
     * @param DestroyRequest $request
     * @return array|mixed
     */
    public function encodeForCancellingInsurance(Insurance $insurance, DestroyRequest $request)
    {
        $insurerRequest = [];

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API request structure which is part of NDA
        // Removed part of code prepares $insurerRequest based on data from $insurance and from $request.
        // $insurerRequest may be an array, json string or XML depending on the insurer API
        // Some fake data below:

        $insurerRequest['insurance_number'] = $insurance->number;

        return $insurerRequest;
    }
}
