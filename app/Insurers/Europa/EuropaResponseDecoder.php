<?php


namespace App\Insurers\Europa;


use App\Contracts\InsurerResponseDecoder;
use App\Models\Insurance;
use App\Models\Offer;
use App\Objects\PolicyPrice;

class EuropaResponseDecoder implements InsurerResponseDecoder
{
    /**
     * @param $response
     * @return PolicyPrice
     */
    public function decodeToPolicyPrice($response): PolicyPrice
    {
        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API response structure which is part of NDA
        // Removed part of code gathers necessary data ($amount & $currency) from insurer API web service
        // response ($response) and basing on that data creates PolicyPrice object.

        $amount = $response['premium']['amount'];
        $currency = $response['premium']['currency_code'];

        return new PolicyPrice($amount, $currency);
    }

    /**
     * @param $response
     * @return Offer
     */
    public function decodeToOffer($response): Offer
    {
        $offer = new Offer();

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API response structure which is part of NDA
        // Removed part of code gathers necessary data from insurer API web service response ($response)
        // and basing on that data fills new Offer model.

        $offer->number = $response['offer']['number'];
        // Setting the rest of the Offer model columns...

        return $offer;
    }

    /**
     * @param $response
     * @return Insurance
     */
    public function decodeToInsurance($response): Insurance
    {
        $insurance = new Insurance();

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API response structure which is part of NDA
        // Removed part of code gathers necessary data from insurer API web service response ($response)
        // and basing on that data fills new Insurance model.

        $insurance->number = $response['insurance']['number'];
        $insurance->url = $response['docs']['policy'];
        // Setting the rest of the Insurance model columns...

        return $insurance;
    }

    /**
     * @param $response
     * @return bool
     */
    public function decodeToCancellationResult($response): bool
    {
        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API response structure which is part of NDA
        // Removed part of code examines insurer API web service response ($response) to determine whether
        // the action was successful. Depending on the implementation it may work like:

        return $response['status'] === 'OK';
    }
}
