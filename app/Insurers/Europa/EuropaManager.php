<?php


namespace App\Insurers\Europa;


use App\Contracts\InsurerManager;
use App\Contracts\InsurerRequestEncoder;
use App\Contracts\InsurerResponseDecoder;
use App\Contracts\InsurerWebServiceClient;

class EuropaManager implements InsurerManager
{
    /**
     * @return InsurerRequestEncoder
     */
    public function getInsurerRequestEncoder(): InsurerRequestEncoder
    {
        return new EuropaRequestEncoder();
    }

    /**
     * @return InsurerWebServiceClient
     */
    public function getWebServiceClient(): InsurerWebServiceClient
    {
        return new EuropaWebServiceClient();
    }

    /**
     * @return InsurerResponseDecoder
     */
    public function getInsurerResponseDecoder(): InsurerResponseDecoder
    {
        return new EuropaResponseDecoder();
    }
}
