<?php


namespace App\Insurers\Europa;


use App\Contracts\HasTokenContract;
use App\Contracts\InsurerWebServiceClient;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\StreamInterface;

class EuropaWebServiceClient implements InsurerWebServiceClient, HasTokenContract
{
    /**
     * Configuration data from config file
     *
     * @var array
     */
    private $config;

    /**
     * Http client
     *
     * @var GuzzleHttpClient
     */
    private $guzzleClient;

    public function __construct()
    {
        $this->config = config('insurer_europa');

        $this->guzzleClient = new GuzzleHttpClient([
            'base_uri'  => $this->config['base_url'],
        ]);
    }

    /**
     * @param $request
     * @return StreamInterface
     * @throws GuzzleException
     */
    public function getPolicyPrice($request)
    {
        return $this->makePostRequest(
            $this->config['endpoints']['policy_pricing'],
            $request
        );
    }

    /**
     * @param $request
     * @return StreamInterface
     * @throws GuzzleException
     */
    public function createPolicyOffer($request)
    {
        return $this->makePostRequest(
            $this->config['endpoints']['policy_offer'],
            $request
        );
    }

    /**
     * @param $request
     * @return StreamInterface
     * @throws GuzzleException
     */
    public function createInsurance($request)
    {
        return $this->makePostRequest(
            $this->config['endpoints']['policy_buy'],
            $request
        );
    }

    /**
     * @param $request
     * @return StreamInterface
     * @throws GuzzleException
     */
    public function cancelInsurance($request)
    {
        return $this->makePostRequest(
            $this->config['endpoints']['policy_cancel'],
            $request
        );
    }

    /**
     * Get authorization token from cache. Obtain it from API if expired
     *
     * @return string
     * @throws GuzzleException
     */
    public function getToken(): string
    {
        if(Cache::missing('europa_token')) {

            $response = json_decode(
                $this->guzzleClient->request('GET', $this->config['endpoints']['initialize'], [
                    'auth' => $this->getAuth(),
                    'query' => ['customer_id' => $this->config['customer_id']],
                ])->getBody(),
                true
            );

            // Token is valid 30 minutes. Use 29 to prevent edge cases
            $expiresAt = now()->addMinutes(29);

            Cache::put('europa_token', $response['token'], $expiresAt);
        }

        return Cache::get('europa_token');
    }

    /**
     * @param $url
     * @param $request
     * @return StreamInterface
     * @throws GuzzleException
     */
    private function makePostRequest($url, $request) {
        return json_decode(
            $this->guzzleClient->request('POST', $url, [
                'headers' => $this->getHeaders(),
                'json' => $request
            ])->getBody(),
            true
        );
    }

    /**
     * @return array
     */
    private function getAuth()
    {
        return [
            $this->config['login'],
            $this->config['password'],
        ];
    }

    /**
     * @return string[]
     * @throws GuzzleException
     */
    private function getHeaders()
    {
        return [
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . $this->getToken(),
        ];
    }
}
