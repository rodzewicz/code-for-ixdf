<?php


namespace App\Services;


use App\Exceptions\InternalApiException;
use App\Factories\InsurerManagerFactory;
use App\Http\Requests\Insurance\DestroyRequest;
use App\Http\Requests\Insurance\StoreRequest;
use App\Models\Insurance;
use App\Models\Offer;
use \Exception;

class InsuranceService
{
    /**
     * Makes API call for insurance creation to insurer's server
     * and stores obtained response in database
     *
     * @param StoreRequest $request
     * @return Insurance
     * @throws InternalApiException
     */
    public function create(StoreRequest $request)
    {
        $offer = Offer::findOrFail($request->offer_id);

        $insurerManager = InsurerManagerFactory::make($offer->policy->insurer);

        $insurerRequest = $insurerManager
            ->getInsurerRequestEncoder()
            ->encodeForCreatingInsurance($offer, $request);

        $insurerResponse = $insurerManager
            ->getWebServiceClient()
            ->createInsurance($insurerRequest);

        $insurance = $insurerManager
            ->getInsurerResponseDecoder()
            ->decodeToInsurance($insurerResponse);

        $insurance
            ->offer()
            ->associate($offer)
            ->save();

        return $insurance;
    }

    /**
     * Makes API call for insurance cancellation to insurer's server
     * and removes insurance entry from database
     *
     * @param Insurance $insurance
     * @param DestroyRequest $request
     * @return bool|null
     * @throws InternalApiException
     * @throws Exception
     */
    public function destroy(Insurance $insurance, DestroyRequest $request)
    {
        $insurerManager = InsurerManagerFactory::make($insurance->offer->policy->insurer);

        $insurerRequest = $insurerManager
            ->getInsurerRequestEncoder()
            ->encodeForCancellingInsurance($insurance, $request);

        $insurerResponse = $insurerManager
            ->getWebServiceClient()
            ->cancelInsurance($insurerRequest);

        $result = $insurerManager
            ->getInsurerResponseDecoder()
            ->decodeToCancellationResult($insurerResponse);

        if(!$result) {
            throw new InternalApiException('Cannot cancel policy');
        }

        return $insurance->delete();
    }
}
