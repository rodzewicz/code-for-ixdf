<?php


namespace App\Services;


use App\Factories\InsurerManagerFactory;
use App\Http\Requests\Policy\StoreOfferRequest;
use App\Models\Policy;
use App\Models\Offer;
use App\Exceptions\InternalApiException;

class OfferService
{
    /**
     * Makes API call for policy offer to insurer's server
     * and stores obtained response in database
     *
     * @param Policy $policy
     * @param StoreOfferRequest $request
     * @return Offer
     * @throws InternalApiException
     */
    public function create(Policy $policy, StoreOfferRequest $request)
    {
        $insurerManager = InsurerManagerFactory::make($policy->insurer);

        $insurerRequest = $insurerManager
            ->getInsurerRequestEncoder()
            ->encodeForCreatingPolicyOffer($policy, $request);

        $insurerResponse = $insurerManager
            ->getWebServiceClient()
            ->createPolicyOffer($insurerRequest);

        $offer = $insurerManager
            ->getInsurerResponseDecoder()
            ->decodeToOffer($insurerResponse);

        $offer
            ->policy()
            ->associate($policy)
            ->save();

        return $offer;
    }
}
