<?php


namespace App\Services;


use App\Factories\InsurerManagerFactory;
use App\Http\Requests\Policy\ShowPriceRequest;
use App\Models\Policy;
use App\Exceptions\InternalApiException;
use App\Objects\PolicyPrice;

class PolicyPriceService
{
    /**
     * Makes API call for policy price to insurer's server
     *
     * @param Policy $policy
     * @param ShowPriceRequest $request
     * @return PolicyPrice
     * @throws InternalApiException
     */
    public function get(Policy $policy, ShowPriceRequest $request)
    {
        $insurerManager = InsurerManagerFactory::make($policy->insurer);

        $insurerRequest = $insurerManager
            ->getInsurerRequestEncoder()
            ->encodeForGettingPolicyPrice($policy, $request);

        $insurerResponse = $insurerManager
            ->getWebServiceClient()
            ->getPolicyPrice($insurerRequest);

        return $insurerManager
            ->getInsurerResponseDecoder()
            ->decodeToPolicyPrice($insurerResponse);
    }
}
