<?php


namespace App\Factories;


use App\Exceptions\InternalApiException;
use App\Insurers\Europa\EuropaManager;
use App\Models\Insurer;

class InsurerManagerFactory
{
    const EUROPA = 1;
    const SIGNAL = 2;
    const AXA = 3;

    /**
     * @param Insurer $insurer
     * @return EuropaManager
     * @throws InternalApiException
     */
    public static function make(Insurer $insurer)
    {
        if($insurer->id === self::EUROPA) {
            return new EuropaManager();
        }
//        elseif ($insurer->id === self::SIGNAL) {
//            return new SignalManager();
//        }
//        elseif ($insurer->id === self::AXA) {
//            return new AxaManager();
//        }
        else {
            throw new InternalApiException('Insurer not implemented');
        }
    }
}
