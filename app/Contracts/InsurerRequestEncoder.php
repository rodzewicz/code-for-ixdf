<?php


namespace App\Contracts;


use App\Http\Requests\Insurance\DestroyRequest;
use App\Http\Requests\Insurance\StoreRequest;
use App\Http\Requests\Policy\ShowPriceRequest;
use App\Http\Requests\Policy\StoreOfferRequest;
use App\Models\Insurance;
use App\Models\Offer;
use App\Models\Policy;

interface InsurerRequestEncoder
{
    /**
     * Using data from $policy and $request objects, prepare structure required by the insurer's API
     * for getting policy price
     *
     * @param Policy $policy
     * @param ShowPriceRequest $request
     * @return mixed
     */
    public function encodeForGettingPolicyPrice(Policy $policy, ShowPriceRequest $request);

    /**
     * Using data from $policy and $request objects, prepare structure required by the insurer's API
     * for creating policy offer
     *
     * @param Policy $policy
     * @param StoreOfferRequest $request
     * @return mixed
     */
    public function encodeForCreatingPolicyOffer(Policy $policy, StoreOfferRequest $request);

    /**
     * Using data from $offer and $request objects, prepare structure required by the insurer's API
     * for creating the insurance
     *
     * @param Offer $offer
     * @param StoreRequest $request
     * @return mixed
     */
    public function encodeForCreatingInsurance(Offer $offer, StoreRequest $request);

    /**
     * Using data from $insurance and $request objects, prepare structure required by the insurer's API
     * for cancelling the insurance
     *
     * @param Insurance $insurance
     * @param DestroyRequest $request
     * @return mixed
     */
    public function encodeForCancellingInsurance(Insurance $insurance, DestroyRequest $request);
}
