<?php


namespace App\Contracts;


use App\Models\Insurance;
use App\Models\Offer;
use App\Objects\PolicyPrice;

interface InsurerResponseDecoder
{
    /**
     * Convert insurer's API response to the policy pricing request into the PolicyPrice object
     *
     * @param $response
     * @return PolicyPrice
     */
    public function decodeToPolicyPrice($response): PolicyPrice;

    /**
     * Convert insurer's API response to the policy offer request into the Offer object
     *
     * @param $response
     * @return Offer
     */
    public function decodeToOffer($response): Offer;

    /**
     * Convert insurer's API response to the create insurance request into the Insurance object
     *
     * @param $response
     * @return Insurance
     */
    public function decodeToInsurance($response): Insurance;

    /**
     * Convert insurer's API response to the cancel insurance request into the bool value describing
     * whether request was successful
     *
     * @param $response
     * @return bool
     */
    public function decodeToCancellationResult($response): bool;
}
