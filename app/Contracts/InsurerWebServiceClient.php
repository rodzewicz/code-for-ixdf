<?php


namespace App\Contracts;


interface InsurerWebServiceClient
{
    /**
     * Perform API call to insurer's server to obtain price
     * of the policy provided in the $request
     *
     * @param $request
     * @return mixed
     */
    public function getPolicyPrice($request);

    /**
     * Perform API call to insurer's server to create policy offer
     * according to the parameters provided in the $request
     *
     * @param $request
     * @return mixed
     */
    public function createPolicyOffer($request);

    /**
     * Perform API call to insurer's server to create insurance
     * according to the parameters provided in the $request
     *
     * @param $request
     * @return mixed
     */
    public function createInsurance($request);

    /**
     * Perform API call to insurer's server to cancel insurance
     * according to the parameters provided in the $request
     *
     * @param $request
     * @return mixed
     */
    public function cancelInsurance($request);
}
