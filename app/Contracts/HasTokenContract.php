<?php


namespace App\Contracts;


interface HasTokenContract
{
    /**
     * Obtain token from cache. If token is not present
     * obtain it with API and store in cache before returning
     *
     * @return string
     */
    public function getToken(): string;
}
