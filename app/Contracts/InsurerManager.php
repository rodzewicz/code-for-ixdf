<?php


namespace App\Contracts;


interface InsurerManager
{
    /**
     * Returns object with methods to convert frontend HTTP request
     * or/and parameters from database into insurer's API web service
     * request (json, xml, etc...)
     *
     * @return InsurerRequestEncoder
     */
    public function getInsurerRequestEncoder(): InsurerRequestEncoder;

    /**
     * Returns object with methods to call insurer's API web services
     * May be based for example on the GuzzleClient or SoapClient
     *
     * @return InsurerWebServiceClient
     */
    public function getWebServiceClient(): InsurerWebServiceClient;

    /**
     * Returns object with methods to convert insurer's API web service
     * response into Laravel Models (usually) or other structures used internally
     *
     * @return InsurerResponseDecoder
     */
    public function getInsurerResponseDecoder(): InsurerResponseDecoder;
}
