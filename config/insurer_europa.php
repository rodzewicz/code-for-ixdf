<?php

return [
    'base_url' => env('EUROPA_BASE_URL'),

    'login' => env('EUROPA_LOGIN'),

    'password' => env('EUROPA_PASSWORD'),

    'customer_id' => env('EUROPA_CUSTOMER_ID'),

    'endpoints' => [

        /*** REMOVED DUE TO NDA ***/
        // I removed this part of code as it shows insurer API available web services which is part of NDA
        // Removed part is an associative array that contains information of all insurer's web services endpoints
        // Sample endpoint may look like this:
        'initialize' => 'initialize',
        'policy_pricing' => 'policy/pricing',
        'policy_offer' => 'policy/offer',
        'policy_buy' => 'policy/buy',
        'policy_cancel' => 'policy/cancel',
    ],
];
